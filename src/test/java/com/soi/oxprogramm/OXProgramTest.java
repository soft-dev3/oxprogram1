/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.oxprogramm;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author user
 */
public class OXProgramTest {

    public OXProgramTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCheckPlayerOCol1Win() {
        char table[][] = {{'O', '-', '-'},
        {'O', '-', '-'},
        {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckPlayerOCol2Win() {
        char table[][] = {{'-', 'O', '-'},
        {'-', 'O', '-'},
        {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckPlayerOCol3Win() {
        char table[][] = {{'-', '-', 'O'},
        {'-', '-', 'O'},
        {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckPlayerORow1Win() {
        char table[][] = {{'O', 'O', 'O'},
        {'-', '-', '-'},
        {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 1;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckPlayerORow2Win() {
        char table[][] = {{'-', '-', '-'},
        {'O', 'O', 'O'},
        {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 2;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckPlayerORow3Win() {
        char table[][] = {{'-', '-', '-'},
        {'-', '-', '-'},
        {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        int row = 3;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckPlayerOX1Win() {
        char table[][] = {{'O', '-', '-'},
        {'-', 'O', '-'},
        {'-', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkX1(table, currentPlayer));
    }

    @Test
    public void testCheckPlayerOX2Win() {
        char table[][] = {{'-', '-', 'O'},
        {'-', 'O', '-'},
        {'O', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkX2(table, currentPlayer));
    }

    @Test
    public void testCheckPlayerXX2Win() {
        char table[][] = {{'-', '-', 'X'},
        {'-', 'X', '-'},
        {'X', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkX2(table, currentPlayer));
    }

    @Test
    public void testCheckDraw() {
        int count = 8;
        assertEquals(true, OXProgram.checkDraw(count));
    }

    @Test
    public void testCheckWin() {
        char table[][] = {{'O', '-', '-'},
        {'O', '-', '-'},
        {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        int row = 2;
        assertEquals(true, OXProgram.checkWin(table, currentPlayer, row, col));
    }

    @Test
    public void testCheckNoWin() {
        char table[][] = {{'O', '-', '-'},
        {'O', '-', '-'},
        {'-', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        int row = 2;
        assertEquals(false, OXProgram.checkWin(table, currentPlayer, row, col));
    }

}
